import React, { useState, useEffect } from 'react';

function SalesrecordForm ({ getSale_records }) {
  const [autos, setAutos] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [auto, setAuto] = useState('');
  const [salesperson, setSalesperson] = useState('');
  const [customer, setCustomer] = useState('');
  const [saleprice, setSaleprice] = useState('');

  const handleAutoChange = (event) => {
    const value = event.target.value;
    setAuto(value);
  }

  const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSalesperson(value);
  }

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  }

  const handleSalepriceChange = (event) => {
    const value = event.target.value;
    setSaleprice(value);
  }

  const getAutos = async () => {
    const url = 'http://localhost:8100/api/unsold-automobiles'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAutos(data.autos);
    }
  }

  const getSalespeople = async () => {
    const url = 'http://localhost:8090/api/salespeople/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    }
  }

  const getCustomers = async () => {
    const url = 'http://localhost:8090/api/customers/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.automobile = auto
    data.salesperson = parseInt(salesperson)
    data.customer = parseInt(customer)
    data.sale_price = parseInt(saleprice)

    const salerecordUrl = 'http://localhost:8090/api/salesrecords/'
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }

  };
    const response = await fetch(salerecordUrl, fetchConfig);
    if (response.ok) {
      const newSalerecord = await response.json();
      getSale_records();


      setAuto('');
      setSalesperson('');
      setCustomer('');
      setSaleprice('');

    }

    const postdata = {"sold": true};
    let vin = data.automobile
    const updateinventoryUrl = `http://localhost:8100${vin}`
    const fetchInventory = {
      method: "put",
      body: JSON.stringify(postdata),
      headers: {
        'Content-Type': 'application/json',
      }

    }

    const postresponse = await fetch(updateinventoryUrl, fetchInventory);
    if (response.ok) {
      const updatedAuto = await postresponse.json()
      getAutos();
    }

  }

  useEffect(() => {
    getAutos();
    getSalespeople();
    getCustomers();
  }, []);


    return (
      <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new sale</h1>
          <form onSubmit={handleSubmit} id="create-salesrecord-form">
          <div className="mb-3">
                <select value={auto} onChange={handleAutoChange} required id="auto" name="auto" className="form-select">
                  <option value="">Choose an automobile</option>
                  {autos.map(auto => {
                    return (
                        <option key={auto.id} value={auto.href}>{auto.vin}</option>
                    );
                  })}
                </select>
            </div>
          <div className="mb-3">
                <select value={salesperson} onChange={handleSalespersonChange} required id="salesperson" name="salesperson" className="form-select">
                  <option value="">Choose a sales person</option>
                  {salespeople.map(salesperson => {
                    return (
                        <option key={salesperson.id} value={salesperson.id}>{salesperson.name}</option>
                    );
                  })}
                </select>
            </div>
          <div className="mb-3">
                <select value={customer} onChange={handleCustomerChange} required id="customer" name="customer" className="form-select">
                  <option value="">Choose a customer</option>
                  {customers.map(customer => {
                    return (
                        <option key={customer.id} value={customer.id}>{customer.name}</option>
                    );
                  })}
                </select>
            </div>
            <div className="form-floating mb-3">
                <input value={saleprice} onChange={handleSalepriceChange} placeholder="Sale price" required type="number" name="sale_price" id="sale_price" className="form-control"/>
                <label htmlFor="sale_price">Sale price</label>
              </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
      </div>
      );
}

export default SalesrecordForm;
