import React, { useState, useEffect } from "react"
import { Link } from "react-router-dom";

const ServiceAppointmentList = () => {
    const [service_appointments, setAppointments] = useState([]);
    const [is_finished, setFinished] = useState(false);

    useEffect(() => {
        fetch('http://localhost:8080/api/appointments/')
            .then(response => response.json())
            .then(data => setAppointments(data.service_appointments))
            .catch(e => console.error('error: ', e))
    }, [])

    useEffect(() => {
        fetch('http://localhost:8080/api/appointments/')
            .then(response => response.json())
            .then(data => setFinished(data.service_appointments.is_finished))
            .catch(e => console.error('error: ', e))
    }, [])

    const handleDelete = (appointment) => {
        const hrefComponents = appointment.href.split("/");
        const pk = hrefComponents[hrefComponents.length - 2];
        const appointmentsUrl = `http://localhost:8080/api/appointments/${pk}/`;
        const appointmentHref = appointment.href
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        fetch(appointmentsUrl, fetchConfig)
            .then(response => response.json())
            .then(data => {
                if (data.Deleted) {
                    const currentAppointments = [...service_appointments];
                    setAppointments(currentAppointments.filter(appointment => appointment.href !== appointmentHref));
                }
            })
            .catch(e => console.error('error: ', e))
    };

    const handleFinishedChange = (appointment) => {
        setFinished(current => !current);
        const finishedStatus = {
            "is_finished": { is_finished }
        }
        const hrefComponents = appointment.href.split("/");
        const appointmentHref = appointment.href;
        const pk = hrefComponents[hrefComponents.length - 2];
        const appointmentsUrl = `http://localhost:8080/api/appointments/${pk}/`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(finishedStatus),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        fetch(appointmentsUrl, fetchConfig)
            .then(response => response.json())
            .then(setAppointments(service_appointments.filter(appointment => appointment.href !== appointmentHref)))
    }
    return (
        <>
            <table className="table table-striped align-middle mt-5">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {service_appointments.map(appointment => {
                        return (
                            <tr key={appointment.href}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer_name}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.technician.technician_name}</td>
                                <td>{appointment.reason}</td>
                                <td>
                                    <form>
                                        <button className="btn btn-danger" type="button" onClick={() => handleDelete(appointment)}>{service_appointments ? 'Cancel' : 'Delete'}</button>
                                    </form>
                                </td>
                                <td>
                                    <form>
                                        <button className="btn btn-success" type="button" onClick={() => handleFinishedChange(appointment)}>Finished</button>
                                    </form>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <Link to={"/service/new"}>Create appointment</Link>
        </>
    );
}

export default ServiceAppointmentList;
