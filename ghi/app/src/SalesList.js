import React from "react";

function SalesList({ sale_records, getSale_records }) {

    return (
        <>
        <h1>All Sales</h1>
        <table className=" table table-striped">
          <thead>
            <tr>
              <th>Sales person</th>
              <th>Employee number</th>
              <th>Purchaser name</th>
              <th>Automobile VIN</th>
              <th>Sale price</th>
            </tr>
          </thead>
          <tbody>
            {sale_records.map(sale_record => {
              return (
                <tr key={sale_record.id}>
                  <td>{ sale_record.salesperson.name }</td>
                  <td>{ sale_record.salesperson.employee_number }</td>
                  <td>{ sale_record.customer.name }</td>
                  <td>{ sale_record.automobile.vin }</td>
                  <td>{ sale_record.sale_price }</td>
                </tr>
              );
           })}
          </tbody>
        </table>
        </>
    )
}

export default SalesList;
