import React, { useState, useEffect } from 'react';

const AutomobileList = () => {
    const [auto, setAutomobiles] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8100/api/automobiles/')
            .then(response => response.json())
            .then(data => setAutomobiles(data.autos))
            .catch(e => console.error('error: ', e))
    }, [])
    return (
        <>
            <h1>Vehicle Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {auto.map(auto => {
                        return (
                            <tr key={auto.id}>
                                <td>{auto.vin}</td>
                                <td>{auto.color}</td>
                                <td>{auto.year}</td>
                                <td>{auto.model.name}</td>
                                <td>{auto.model.manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    )
}

export default AutomobileList;