# CarCar

Team:

* Lauren Alpuerto - Service Microservice
* Kennedy Cassiday - Sales Microservice

## How to Run this Application:
1. Fork Project Beta
3. Copy the 'Clone with HTTPS' url
4. Open the terminal and cd into your project directory
5. Git clone the project into your project directory
7. Cd into project-beta
8. Type the following commands in the terminal, one by one:
    - docker volume create beta-data
    - docker-compose build
    - docker-compose up
9. In your browser go to [http://localhost:3000/](http://localhost:3000/) to view the front-end of the project.

## Application Diagram:
[Link to Diagram](https://excalidraw.com/#json=FhYqKp-J289elSsOykgUY,Y8_QSp5tIEvSDXUc6v27LQ)

## API Documentation
### Manufacturer
| Action | Method | URL  | JSON Request Body  | Returns (Status Code 200)
|-----------------------|:-------------|:---------------:|---------------:|---------------:|
| List manufacturers | GET  | http://<span></span>localhost:8100/api/manufacturers/ | | ```{"manufacturers": [{"href": "/api/manufacturers/1/", "id": 1, "name": "Honda"}]}``` |
| Create a manufacturer | POST | http://<span></span>localhost:8100/api/manufacturers/ | ```{"name": "Honda"}``` | ```{"href": "/api/manufacturers/1/", "id": 1, "name": "Honda"}``` |
| Show details for a specific manufacturer | GET | http://<span></span>localhost:8100/api/manufacturers/:id/ | | ```{"href": "/api/manufacturers/1/", "id": 1, "name": "Honda"}``` |
| Update a specific manufacturer | PUT | http://<span></span>localhost:8100/api/manufacturers/:id/ | ```{"name": "Toyota"}``` | ```{"href": "/api/manufacturers/1/", "id": 1, "name": "Toyota"}``` |
| Delete a specific manufacturer | DELETE | http://<span></span>localhost:8100/api/manufacturers/:id/ | | ```{"id": null, "name": "Honda"}``` |

### Vehicle Model
| Action | Method | URL  | JSON Request Body  | Returns (Status Code 200)
|-----------------------|:-------------|:---------------:|---------------:|---------------:|
| List vehicle models | GET  | http://<span></span>localhost:8100/api/models/ | | ```{"models": [{"href": "/api/models/1/", "id": 1, "name": "Element", "picture_url": "https://i2.cdn.turner.com/money/2011/08/05/autos/honda_recall/2008-honda-element.top.jpg", "manufacturer": {"href": "/api/manufacturers/1/", "id": 1, "name": "Honda"}}]}``` |
| Create a vehicle model | POST | http://<span></span>localhost:8100/api/models/ | ```{"name": "Element", "picture_url": "https://i2.cdn.turner.com/money/2011/08/05/autos/honda_recall/2008-honda-element.top.jpg", "manufacturer_id": 1}``` | ```{"href": "/api/models/1/", "id": 1, "name": "Element", "picture_url": "https://i2.cdn.turner.com/money/2011/08/05/autos/honda_recall/2008-honda-element.top.jpg", "manufacturer": {"href": "/api/manufacturers/1/", "id": 1, "name": "Honda"}}``` |
| Show details for a specific vehicle model | GET | http://<span></span>localhost:8100/api/models/:id/ | | ```{"href": "/api/models/1/", "id": 1, "name": "Element", "picture_url": "https://i2.cdn.turner.com/money/2011/08/05/autos/honda_recall/2008-honda-element.top.jpg", "manufacturer": {"href": "/api/manufacturers/1/", "id": 1, "name": "Honda"}}``` |
| Update a specific vehicle model | PUT | http://<span></span>localhost:8100/api/models/:id/ | ```{"name": "Accord"}``` | ```{"href": "/api/models/1/", "id": 1, "name": "Accord", "picture_url": "https://i2.cdn.turner.com/money/2011/08/05/autos/honda_recall/2008-honda-element.top.jpg", "manufacturer": {"href": "/api/manufacturers/1/", "id": 1, "name": "Honda"}}```
| Delete a specific vehicle model| DELETE | http://<span></span>localhost:8100/api/models/:id/ | | ```{"id": null, "name": "Element", "picture_url": "https://i2.cdn.turner.com/money/2011/08/05/autos/honda_recall/2008-honda-element.top.jpg", "manufacturer": {"href": "/api/manufacturers/1/", "id": 1, "name": "Honda"}}``` |

### Automobile
| Action | Method | URL  | JSON Request Body | Returns (Status Code 200)
|-----------------------|:-------------|:---------------:|---------------:|---------------:|
| List automobiles | GET  | http://<span></span>localhost:8100/api/automobiles/ | | ```{"autos": [{"href": "/api/automobiles/1G1AT58H097267302/", "id": 1, "color": "Orange", "year": 2011, "vin": "1G1AT58H097267302", "model": {"href": "/api/models/1/", "id": 1, "name": "Element", "picture_url": "https://i2.cdn.turner.com/money/2011/08/05/autos/honda_recall/2008-honda-element.top.jpg", "manufacturer": {"href": "/api/manufacturers/1/", "id": 1, "name": "Honda"}}, "sold": false}]}``` |
| Create an automobile | POST | http://<span></span>localhost:8100/api/automobiles/ | ```{"color": "Orange", "year": 2011, "vin": "1G1AT58H097267302", "model_id": 1}``` | ```{"href": "/api/automobiles/1G1AT58H097267302/", "id": 1, "color": "Orange", "year": 2011, "vin": "1G1AT58H097267302", "model": {"href": "/api/models/1/", "id": 1, "name": "Element", "picture_url": "https://i2.cdn.turner.com/money/2011/08/05/autos/honda_recall/2008-honda-element.top.jpg", "manufacturer": {"href": "/api/manufacturers/1/", "id": 1, "name": "Honda"}}, "sold": false}```
| Show details for a specific automobile | GET | http://<span></span>localhost:8100/api/automobiles/:vin/ | | ```{"href": "/api/automobiles/1G1AT58H097267302/", "id": 1, "color": "Orange", "year": 2011, "vin": "1G1AT58H097267302", "model": {"href": "/api/models/1/", "id": 1, "name": "Element", "picture_url": "https://i2.cdn.turner.com/money/2011/08/05/autos/honda_recall/2008-honda-element.top.jpg", "manufacturer": {"href": "/api/manufacturers/1/", "id": 1, "name": "Honda"}}, "sold": false}``` |
| Update a specific automobile | PUT | http://<span></span>localhost:8100/api/automobiles/:vin/ | ```{"color": "Magenta"}``` | ```{"href": "/api/automobiles/1G1AT58H097267302/", "id": 1, "color": "Magenta", "year": 2011, "vin": "1G1AT58H097267302", "model": {"href": "/api/models/1/", "id": 1, "name": "Element", "picture_url": "https://i2.cdn.turner.com/money/2011/08/05/autos/honda_recall/2008-honda-element.top.jpg", "manufacturer": {"href": "/api/manufacturers/1/", "id": 1, "name": "Honda"}}, "sold": false}``` |
| Delete a specific automobile | DELETE | http://<span></span>localhost:8100/api/automobiles/:vin/ | | ```{"href": "/api/automobiles/1G1AT58H097267302/", "id": null, "color": "Orange", "year": 2011, "vin": "1G1AT58H097267302", "model": {"href": "/api/models/1/", "id": 1, "name": "Element", "picture_url": "https://i2.cdn.turner.com/money/2011/08/05/autos/honda_recall/2008-honda-element.top.jpg", "manufacturer": {"href": "/api/manufacturers/1/", "id": 1, "name": "Honda"}}, "sold": false}``` |

### Sales
| Action | Method | URL  | JSON Request Body  | Returns (Status Code 200)
|-----------------------|:-------------|:---------------:|---------------:|---------------:|
| List salespeople | GET  | http://<span></span>localhost:8090/api/salespeople/ | | ```{"salespeople": [{"name": "Tommy Shelby", "employee_number": "1", "id": 1}]}``` |
| Create a sales person | POST | http://<span></span>localhost:8090/api/salespeople/ | ```{"name": "Tommy Shelby", "employee_number": "1"}``` | ```{"name": "Tommy Shelby", "employee_number": "1", "id": 1}``` |
| Show details for a specific sales person | GET | http://<span></span>localhost:8090/api/salespeople/:id/ | | ```{"name": "Tommy Shelby", "employee_number": "1", "id": 1}``` |
| List customers | GET  | http://<span></span>localhost:8090/api/customers/ | | ```{"customers": [{"name": "Vito Corleone", "address": "3531 White Plains Road, Bronx, New York City, New York, USA", "phone_number”: "444-444-4444", "id": 1}]}``` |
| Create a customer | POST | http://<span></span>localhost:8090/api/customers/ | ```{"name": "Vito Corleone", "address": "3531 White Plains Road, Bronx, New York City, New York, USA", "phone_number": "444-444-4444"}``` | ```{"name": "Vito Corleone", "address": "3531 White Plains Road, Bronx, New York City, New York, USA", "phone_number”: "444-444-4444", "id": 1}``` |
| Show details for a specific customer | GET | http://<span></span>localhost:8090/api/customers/:id/ | | ```{"name": "Vito Corleone", "address": "3531 White Plains Road, Bronx, New York City, New York, USA", "phone_number”: "444-444-4444", "id": 1}``` |
| List sale records | GET  | http://<span></span>localhost:8090/api/salesrecords/ | | ```{"sale_records": [{"sale_price": 11500, "salesperson": {"name": "Tommy Shelby", "employee_number": "1", "id": 1}, "customer": {"name": "Vito Corleone", "address": "3531 White Plains Road, Bronx, New York City, New York, USA", "phone_number”: "444-444-4444", "id": 1}, "automobile": {"vin": "1G1AT58H097267302", "sold": true, "import_href": "/api/automobiles/1G1AT58H097267302/"}, "id": 1}]}``` |
| Create a sale record | POST | http://<span></span>localhost:8090/api/salesrecords/ | ```{"sale_price": 11500, "salesperson": 1, "customer": 1, "automobile": "/api/automobiles/1G1AT58H097267302/"}``` | ```{"sale_price": 11500, "salesperson": {"name": "Tommy Shelby", "employee_number": "1", "id": 1}, "customer": {"name": "Vito Corleone", "address": "3531 White Plains Road, Bronx, New York City, New York, USA", "phone_number”: "444-444-4444", "id": 1}, "automobile": {"vin": "1G1AT58H097267302", "sold": true, "import_href": "/api/automobiles/1G1AT58H097267302/"}, "id": 1}``` |
| Show details for a specific sale record | GET | http://<span></span>localhost:8090/api/salesrecords/:id/ | | ```{"sale_price": 11500, "salesperson": {"name": "Tommy Shelby", "employee_number": "1", "id": 1}, "customer": {"name": "Vito Corleone", "address": "3531 White Plains Road, Bronx, New York City, New York, USA", "phone_number”: "444-444-4444", "id": 1}, "automobile": {"vin": "1G1AT58H097267302", "sold": true, "import_href": "/api/automobiles/1G1AT58H097267302/"}, "id": 1}``` |
| List available (unsold) automobiles | GET  | http://<span></span>localhost:8100/api/unsold-automobiles | | ```{"autos": [{"href": "/api/automobiles/1G1AT58H097267302/", "id": 1, "color": "Orange", "year": 2011, "vin": "1G1AT58H097267302", "model": {"href": "/api/models/1/", "id": 1, "name": "Element", "picture_url": "https://i2.cdn.turner.com/money/2011/08/05/autos/honda_recall/2008-honda-element.top.jpg", "manufacturer": {"href": "/api/manufacturers/1/", "id": 1, "name": "Honda"}}, "sold": false}]}``` |

## Service
| Action | Method | URL  | JSON Request Body  | Returns (Status Code 200)
|-----------------------|:-------------|:---------------:|---------------:|---------------:|
| List appointments | GET  | http://<span></span>localhost:8080/api/appointments/ | | ```{"service_appointments": [{"href": "/api/appointments/1/", "id": 1, "vin": "12345678912345678", "customer_name": "hi", "date": "2023-01-01", "time": "20:00:00", "reason": "whatever", "technician": { "href": "/api/technicians/1/", "id": 1, "technician_name": "lauren", "employee_number": 234}, "vip": false, "is_finished": false, "cancelled": false}]}``` |
| Enter an appointment | POST | http://<span></span>localhost:8080/api/appointments/ | ```{"vin": "12345678912345678", "customer_name": "hi", "date": "2023-01-01", "time": "20:00", "reason": "whatever","technician": 234424}``` | ```{"href": "/api/appointments/1/", "id": 1, "vin": "12345678912345678", "customer_name": "hi", "date": "2023-01-01","time": "20:00", "reason": "whatever", "technician": {"href": "/api/technicians/1/", "id": 1,"technician_name": "lauren", "employee_number": 234}, "vip": false, "is_finished": false, "cancelled": false}``` |
| Show an appointment | GET | http://<span></span>localhost:8080/api/appointments/:id/ | | ```{"href": "/api/appointments/1/", "id": 1, "vin": "12345678912345678", "customer_name": "hi", "date": "2023-01-01", "time": "20:00:00", "reason": "whatever", "technician": {"href": "/api/technicians/1/", "id": 1, "technician_name": "lauren", "employee_number": 234}, "vip": false, "is_finished": false, "cancelled": false}``` |
| Update an appointment | PUT  | http://<span></span>localhost:8080/api/appointments/:id/ | ```{"vin": "12345678912345678", "customer_name": "hi", "date": "2023-01-01", "time": "20:00:00", "reason": "whatever", "technician_technician_name": "me", "technician_employee_number": 234424}``` | ```{"href": "/api/appointments/6/", "id": 6, "vin": "12345678912345678", "customer_name": "hi", "date": "2023-01-01", "time": "20:00:00", "reason": "whatever", "technician": {"href": "/api/technicians/10/", "id": 10, "technician_name": "me", "employee_number": 234424}, "vip": false, "is_finished": false,"cancelled": false}``` |
| Delete an appointment | DEL | http://<span></span>localhost:8080/api/appointments/:id/ | ```{"Deleted": true}``` |
| List technicians | GET | http://<span></span>localhost:8080/api/technicians/ | | ```{"technicians": [{"href": "/api/technicians/2/", "id": 2, "technician_name": "Lauren Alpuerto", "employee_number": 5432}, {"href": "/api/technicians/1/", "id": 1, "technician_name": "iggy", "employee_number": 234}]}``` |
| Enter a technician | POST  | http://<span></span>localhost:8080/api/technicians/ | ```{"technician_name": "me", "employee_number": 7567435}``` | ```{"href": "/api/technicians/10/", "id": 10, "technician_name": "me", "employee_number": 234424}``` |
| Show technician | GET | http://<span></span>localhost:8080/api/technicians/:id/ | ```{"href": "/api/technicians/1/", "id": 1, "technician_name": "iggy", "employee_number": 234}``` |
| Update a technician | PUT | http://<span></span>localhost:8080/api/technicians/:id/ | ```{"technician_name": "hey"}``` | ```{"href": "/api/technicians/1/", "id": 1, "technician_name": "hey", "employee_number": 234}``` |
| Service history by VIN | GET  | http://<span></span>localhost:8080/api/servicehistory/vin/str:pk>/ | | ```{"href": "/api/appointments/7/", "id": 7, "vin": "1C3CC5FB2AN120174","customer_name": "la", "date": "2023-01-04", "time": "14:52:00", "reason": "tire rotation", "technician": {"href": "/api/technicians/5/", "id": 5, "technician_name": "lauren", "employee_number": 32552}}``` |

## Design
Within this application are three microservices: **Service**, **Inventory**, and **Sales**. Inventory manages the automobiles in inventory. Sales manages the sale process, which includes the sales people, customers, automobiles being sold, and resulting sale records. Service manages the service process, including the technicians, service appointments, and automobiles being serviced.

Both Sales and Service have an Automobile Value Object. They each poll Inventory for data on the automobiles within. This data is used for Service to make service appointments and for Sales to create sale records.

## Service microservice

The Service microservice contains three models, **AutomobileVO**, **Technician**, and  **ServiceAppointment**.

**AutomobileVO**

The poller.py in the service_rest folder polls data from the Inventory API to route information to AutomobileVO.
<br>
Model attributes:
- import_href
- vin
- color
- year
- model-name

**Technician**

A technican can be created by a front-end user. The inputs required are a technician name and employee number. When a technician is created, a service appointment can be attributed to them.
Model attributes:
- technician_name
- employee_number

**ServiceAppointment**

A service appointment can be created by a front-end user. The inputs required are VIN, the customer's name, date, time, technician, and reason.<br>
Model attributes:
- vin
- customer_name
- date
- time
- reason
- vip
- is_finished
- cancelled
-technician

## Sales microservice

The Sales microservice contains four models, **AutomobileVO**, **Salesperson**, **Customer**, and **SaleRecord**.

**AutomobileVO  (Value Object):**
Polls for data from the Automobile model within the Inventory bounded context. The VIN is the data needed from the Automobile model. When a new automobile is created, an AutomobileVO instance is created.
<br>
Model attributes:
- vin (unique value)
- sold (default value is false)
- import_href

**SalesPerson:**
A salesperson can be created by a front-end user. When creating a salesperson, a name and employee number are required. Once a salesperson has been created, an automobile sale may be attributed to them.<br>
Model attributes:
- name
- employee_number

**Customer:**
A customer can be created by a front-end user. When creating a customer, a name, address, and phone number are required. Once a customer has been created, they may be sold an automobile.<br>
Model attributes:
- name
- address
- phone_number

**SaleRecord:**
A sale record can be created by a front-end user. When creating a sale record, a sale price must be input and an existing salesperson, customer and automobile selected. Once a sale record has been submitted, the automobile recorded upon it is marked as ‘sold’ and it cannot be sold again.<br>
Model attributes:
- sale_price
- salesperson (ForeignKey referencing the SalesPerson model)
- customer (ForeignKey referencing the Customer model)
- automobile (ForeignKey referencing the AutomobileVO model)
