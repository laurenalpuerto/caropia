from django.shortcuts import render
from .models import Salesperson, Customer, AutomobileVO, SaleRecord
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .encoders import (
    SalespersonListEncoder,
    SalespersonDetailEncoder,
    CustomerListEncoder,
    CustomerDetailEncoder,
    SaleRecordListEncoder,
    SaleRecordDetailEncoder,
    )

@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonListEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def api_show_salespeople(request, id):
    if request.method == "GET":
        salesperson = Salesperson.objects.get(id=id)
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Salesperson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def api_show_customers(request, id):
    if request.method == "GET":
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})



@require_http_methods(["GET", "POST"])
def api_list_salesrecords(request, automobile_vo_id=None):
    if request.method == "GET":
        if automobile_vo_id is not None:
            sale_records = SaleRecord.objects.filter(automobile=automobile_vo_id)
        else:
            sale_records = SaleRecord.objects.all()
        return JsonResponse(
            {"sale_records":sale_records},
            encoder=SaleRecordListEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            salesperson = Salesperson.objects.get(id=content["salesperson"])
            content["salesperson"] = salesperson

        except Salesperson.DoesNotExist:
           return JsonResponse(
               {"message": "Invalid salesperson id"},
               status=400
           )
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer

        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=400
            )

        try:
            automobile_href = content["automobile"]
            automobile = AutomobileVO.objects.get(import_href=automobile_href)
            automobile.sold = True
            automobile.save()
            content["automobile"] = automobile


        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile href"},
                status=400
            )

        sale_record = SaleRecord.objects.create(**content)
        return JsonResponse(
            sale_record,
            encoder=SaleRecordDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def api_show_salesrecord(request, id):
    if request.method == "GET":
        salerecord = SaleRecord.objects.get(id=id)
        return JsonResponse(
            salerecord,
            encoder=SaleRecordDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = SaleRecord.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
