from django.contrib import admin

from .models import AutomobileVO, Salesperson, Customer, SaleRecord

@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    pass

@admin.register(Salesperson)
class Salesperson(admin.ModelAdmin):
    pass

@admin.register(Customer)
class Customer(admin.ModelAdmin):
    pass

@admin.register(SaleRecord)
class SaleRecord(admin.ModelAdmin):
    pass
