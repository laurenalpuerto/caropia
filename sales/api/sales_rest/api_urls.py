from django.urls import path
from .views import (
    api_list_salespeople,
    api_show_salespeople,
    api_list_customers,
    api_show_customers,
    api_list_salesrecords,
    api_show_salesrecord,
)

urlpatterns = [
    path("salespeople/", api_list_salespeople, name="api_list_salespeople"),
    path("salespeople/<int:id>/", api_show_salespeople, name="api_show_salespeople"),
    path("customers/", api_list_customers, name="api_list_customers"),
    path("customers/<int:id>/", api_show_customers, name="api_show_customers"),
    path("salesrecords/", api_list_salesrecords, name="api_list_salesrecords"),
    path("salesrecords/<int:id>/", api_show_salesrecord, name="api_show_salesrecord"),
]
