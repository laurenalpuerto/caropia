from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False, null=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)


class Salesperson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name


class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=20)

    def __str__(self):
        return self.name

class SaleRecord(models.Model):
    sale_price = models.IntegerField(null=True)
    salesperson = models.ForeignKey(
        "Salesperson",
        related_name="salerecords",
        on_delete=models.PROTECT,
        null=True,
    )

    customer = models.ForeignKey(
        "Customer",
        related_name="salerecords",
        on_delete=models.PROTECT,
        null=True,
    )

    automobile = models.ForeignKey(
        "AutomobileVO",
        related_name="salerecords",
        on_delete=models.PROTECT,
        null=True,
    )
