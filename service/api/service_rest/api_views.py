from django.shortcuts import render
from service_rest.models import ServiceAppointment, Technician, AutomobileVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json

# Create your views here.


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "import_href",
        "vin",
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "technician_name",
        "employee_number",
    ]


class ServiceAppointmentEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "date",
        "time",
        "reason",
        "technician",
        "vip",
        "is_finished",
        "cancelled",
    ]
    encoders = {"technician": TechnicianEncoder()}


class HistoryEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "date",
        "time",
        "reason",
        "technician",
    ]
    encoders = {"technician": TechnicianEncoder()}


@require_http_methods(["GET"])  # separate view by VIN
def api_service_history(request, pk):
    if request.method == "GET":
        history = ServiceAppointment.objects.get(vin=pk)
        if history == []:
            return JsonResponse(
                {"message": "Invalid vin"},
                status=404,
                safe=False,
            )
        else:
            return JsonResponse(
                history,
                encoder=HistoryEncoder,
                safe=False,
            )


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            TechnicianEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Technician could not be created"},
                status=400,
            )


@require_http_methods(["GET", "PUT"])
def api_show_technicians(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician could not be found"},
                status=400,
            )
    else:
        content = json.loads(request.body)
        Technician.objects.filter(id=pk).update(**content)
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_service_appointments(request):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"service_appointments": appointments},
            encoder=ServiceAppointmentEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            check_number = content["technician"]
            technician = Technician.objects.get(employee_number=check_number)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician could not be found"},
                status=400,
            )

        if hasattr(content, "vip"):
            del content["vip"]
        if hasattr(content, "is_finished"):
            del content["is_finished"]

        appointment = ServiceAppointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=ServiceAppointmentEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        appointment = ServiceAppointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=ServiceAppointmentEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = ServiceAppointment.objects.filter(id=pk).delete()
        return JsonResponse({"Deleted": count > 0})
    else:
        try:
            content = json.loads(request.body)
            appointment = ServiceAppointment.objects.get(id=pk)
            if "technician" in content:
                try:
                    technician = Technician.objects.get(
                        employee_number=content["technician"]
                    )
                    setattr(appointment, "technician", technician)
                except Technician.DoesNotExist:
                    return JsonResponse(
                        {"message": "Invalid employee number"},
                        status=400,
                    )
            return JsonResponse(
                appointment,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )
        except ServiceAppointment.DoesNotExist:
            return JsonResponse(
                {"message": "Service appointment doesn't exist"},
                status=400,
            )
