from django.db import models
from django.urls import reverse

# Create your models here.


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=20, blank=True, null=True, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    color = models.CharField(max_length=200)
    year = models.PositiveIntegerField(blank=False)
    model_name = models.CharField(max_length=200)

    def __str__(self):
        return f"{self.year} {self.model_name} {self.color}"


class Technician(models.Model):
    technician_name = models.CharField(max_length=200)
    employee_number = models.PositiveIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_show_technicians", kwargs={"pk": self.pk})

    def __str__(self):
        return self.technician_name


class ServiceAppointment(models.Model):
    vin = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=200)
    date = models.DateField(null=True)
    time = models.TimeField(null=True)
    reason = models.CharField(max_length=200)
    vip = models.BooleanField(default=False)
    is_finished = models.BooleanField(default=False)
    cancelled = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="services",
        on_delete=models.PROTECT,
    )

    def set_vip_status(self, *args, **kwargs):
        dealership_car = AutomobileVO.objects.filter(vin=self.vin)
        self.vip = len(dealership_car) > 0
        super().save(*args, **kwargs)

    def set_finished_status(self):
        self.is_finished = not self.is_finished
        self.save()

    def change_appointment_status(self):
        self.cancelled = not self.cancelled
        self.save()

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.customer_name}'s service appointment. VIP Status = {self.vip}"
